class Routes {
  static const String root = '/';
  static const String login = '/login';
  static const String register = '/register';
  static const String profile = '/profile';
  static const String main = '/main';
  static const String note = '/note';
  static const String noteEditor = '/noteEditor';
  static const String todo = '/todo';
}