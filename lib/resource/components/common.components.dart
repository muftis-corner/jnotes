import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../color.dart';

class Spacing extends StatelessWidget {
  final double vertical;
  final double horizontal;

  const Spacing({Key key, this.vertical, this.horizontal}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: vertical,
      width: horizontal,
    );
  }
}

class TextFieldFormBlue extends StatelessWidget {
  final TextAlign textAlign;
  final bool password;
  final String placeholder;
  const TextFieldFormBlue({
    Key key,
    this.password = false,
    this.placeholder,
    this.textAlign = TextAlign.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTextField(
      textAlign: textAlign,
      placeholder: placeholder,
      padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
      placeholderStyle:
          TextStyle(color: BaseColor.darkBlue.withOpacity(0.5), fontSize: 20),
      obscureText: password,
      clearButtonMode: OverlayVisibilityMode.editing,
      cursorColor: BaseColor.darkBlue,
      style: TextStyle(color: BaseColor.darkBlue, fontSize: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: textFieldColor),
    );
  }
}

class Button extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String text;
  final Function onPressed;
  const Button({
    Key key,
    this.color = buttonColor,
    this.text = 'Text',
    @required this.onPressed,
    this.textColor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
  onPressed: () =>onPressed(),
  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
  padding: const EdgeInsets.all(0.0),
  child: Ink(
    decoration: BoxDecoration(
      gradient: BackgroundGradient.main,
      borderRadius: BorderRadius.all(Radius.circular(80.0)),
    ),
    child: Container(
      constraints: const BoxConstraints(minWidth: 88.0, minHeight: 40.0), // min sizes for Material buttons
      alignment: Alignment.center,
      child: Text(
        text,
        style: TextStyle(
          color: textColor,
          fontSize: 20
        ),
        textAlign: TextAlign.center,
      ),
    ),
  ),
);
  }
}

class LinkButton extends StatelessWidget {
  final VoidCallback onTap;
  final String label;

  const LinkButton({Key key, @required this.onTap, @required this.label})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
        child: Text(
          label,
          style: TextStyle(color: Theme.of(context).accentColor),
        ),
        onPressed: () => onTap);
  }
}
