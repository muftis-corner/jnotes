import 'package:flutter/material.dart';
import 'package:jnotes/resource/color.dart';

ThemeData appliedTheme = lightTheme;

final ThemeData lightTheme = ThemeData(
  textTheme: TextTheme(
    body1: TextStyle(color:BaseColor.dark),
    body2: TextStyle(color:BaseColor.dark),
    button: TextStyle(color:BaseColor.dark),
    caption: TextStyle(color:BaseColor.dark),
    display1: TextStyle(color:BaseColor.dark),
    display2: TextStyle(color:BaseColor.dark),
    display3:  TextStyle(color:BaseColor.dark),
    display4: TextStyle(color:BaseColor.dark),
    headline: TextStyle(color:BaseColor.dark),
    overline: TextStyle(color:BaseColor.dark),
    subhead: TextStyle(color:BaseColor.dark),
    subtitle: TextStyle(color:BaseColor.dark.withOpacity(0)),
    title : TextStyle(color:BaseColor.dark,fontWeight: FontWeight.w600),    
  ).apply(
    bodyColor: BaseColor.dark,
    displayColor: BaseColor.dark),

  brightness: Brightness.light,
  primaryColor: BaseColor.white,
  primaryColorDark: BaseColor.dark,
  accentColor: BaseColor.bluePastel,
  cursorColor: BaseColor.bluePastel,
  backgroundColor: BaseColor.dark.withOpacity(0.2),
  inputDecorationTheme: InputDecorationTheme(
    fillColor: BaseColor.dark,
    labelStyle: new TextStyle(color: BaseColor.lightGrey),
    border: new UnderlineInputBorder(
        borderSide: new BorderSide(
          color: BaseColor.lightGrey
        ),
      ),
  ),
);

final ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: BaseColor.dark,
  primaryColorDark: BaseColor.white,
  accentColor: BaseColor.bluePastel,
  cursorColor: BaseColor.bluePastel,
  backgroundColor: BaseColor.dark.withOpacity(0.5),
  inputDecorationTheme: InputDecorationTheme(
    fillColor: BaseColor.white,
    labelStyle: new TextStyle(color: BaseColor.lightGrey),
    border: new UnderlineInputBorder(
        borderSide: new BorderSide(
          color: BaseColor.lightGrey
        ),
      ),
  ),
);
