import 'package:flutter/material.dart';

class SideRadius{
  static final card = BorderRadius.circular(15);
}

class Paddings{
  static const top = EdgeInsets.only(top:16);
  static const bottom = EdgeInsets.only(bottom:16);
}