import 'package:flutter/material.dart';

const Color textFieldColor = Color(0xFF76B8E0);
const Color buttonColor = Color(0xFFa5cae4);

Color inverseColor = BaseColor.white;

List<Color> blue = [
  Colors.lightBlueAccent,
  Colors.lightBlue,
  Colors.blue,
  Colors.blueAccent,
];


List<Color> darkBlueGradient = [
  BaseColor.darkBlue,
  Colors.lightBlueAccent,
];

class BaseColor {
  static const Color white = Color.fromRGBO(230, 230, 230, 1);
  static const Color darkBlue = Color.fromRGBO(15, 91, 154, 1);
  static const Color bluePastel = Color(0xff7cb9d0);
  static const Color lightGrey = Color(0xffa8a8a8);
  static const Color dark = Color.fromRGBO(15, 17, 26, 1);
  static const Color shadow = Colors.black26;
}

class BackgroundGradient {
  static LinearGradient main = LinearGradient(
    colors: darkBlueGradient,
    begin: Alignment.topCenter,
    end: Alignment.bottomLeft,
  );
  static LinearGradient reverse = LinearGradient(
    colors: darkBlueGradient,
    begin: Alignment.bottomLeft,
    end: Alignment.topCenter,
  );
}
