// import 'dart:io';

// import 'package:hive/hive.dart';
// import 'package:jnotes/service/Hive/hive.model.dart';
// import 'package:path_provider/path_provider.dart';

// class HiveService{
//   Storage storage= Storage();

//   initHive() async {
//     Directory hiveDirectory = await getApplicationDocumentsDirectory();
//     Hive.init('${hiveDirectory.path}/db');  
//   }

//   initDB() async {
//     Future.value(initHive())
//     .then((_)async{
//       storage.sessionBox = await Hive.openBox('session');
//       storage.notesBox = await Hive.openBox('notes');
//       storage.todoBox = await Hive.openBox('todo');
//       storage.testBox = await Hive.openBox('test');
//     });
//   }
  
//   void initService(){
//     initDB();
//   }
// }