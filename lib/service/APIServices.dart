import 'package:http/http.dart' as http;

class APIService {
  static Future getData(url,{header})async{
    final response = await http.get(url,headers: header);
    return response;
  }
  
  static Future postData(url,body,{header})async{
    final response = await http.post(url,body: body,headers: header);
    return response;
  }
}