import 'package:flutter/material.dart';

void navigatorPush(BuildContext context, Widget nextPage) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => nextPage));
}
void navigatorPushNamed(BuildContext context, String routes){
  Navigator.pushNamed(context, routes);
}
void navigatorPop(BuildContext context){
  Navigator.pop(context);
}
void navigatorPushRoot(BuildContext context, String routes){
  Navigator.pushNamedAndRemoveUntil(
            context, routes, ModalRoute.withName(routes));
}

