import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/modules/notes/page/notes.editor.dart';
import '../../resource/routes.dart';
import '../../service/navigationServices.dart';

import 'components/notes.components.dart';
import 'model/notes.model.dart';

class NotesView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    List<NotesItem> itemList = NotesItem.notesItemList;

    return Scaffold(
      appBar: AppBar(
        title: Text('Note'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          navigatorPushNamed(context, Routes.noteEditor);
        },
        elevation: 5,
        child: Icon(CupertinoIcons.add),
      ),
      body: ListView.builder(
        itemCount: itemList.length,
        itemBuilder: (BuildContext context, int index) {
          return new NoteItem(
            title: itemList[index].title,
            note: itemList[index].note,
            onTap: ()=>navigatorPush(context, NotesEditorView(title: itemList[index].title,note: itemList[index].note,)),
            );
        },
      ),
    );
  }
}

