import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/service/navigationServices.dart';

class NotesEditorView extends StatefulWidget {
  final String title;
  final String note;

  const NotesEditorView({Key key, this.title, this.note}) : super(key: key);
  @override
  _NotesEditorViewState createState() => _NotesEditorViewState();
}

class _NotesEditorViewState extends State<NotesEditorView> {
  TextEditingController titleController;
  TextEditingController notesController; 
  DateTime createdDate = DateTime.now();
  FocusNode notesFocus;
  String abc;

  @override
  void initState() {
    titleController = TextEditingController(text: widget.title);
    notesController = TextEditingController(text: widget.note);
    super.initState();
    notesFocus = FocusNode();
  }

  @override
  void dispose() {
    notesFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(CupertinoIcons.pen),
        onPressed: () {
          navigatorPop(context);
        },
      ),
      appBar: AppBar(
        leading: null,
        automaticallyImplyLeading: true,
        backgroundColor: Colors.grey,
        title: Padding(
          padding: const EdgeInsets.all(16.0),
          child: TextFormField(
            controller: titleController,
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            decoration: InputDecoration.collapsed(hintText: 'Title'),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 0),
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(notesFocus),
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  focusNode: notesFocus,
                  controller: notesController,
                  decoration: InputDecoration.collapsed(hintText: "What's in your mind ?"),
                  maxLines: null,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
