class Notes {
  final int id;
  final String title;
  final String note;
  final String createdDate;
  final String updatedDate;
  final String isDeleted;

  Notes(
      {this.id,
      this.title,
      this.note,
      this.createdDate,
      this.updatedDate,
      this.isDeleted});
}

class NotesItem {
  final int id;
  final String title;
  final String note;

  NotesItem({this.id, this.title, this.note});

  static List<NotesItem> notesItemList = [
    NotesItem(
        id: 0,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
    NotesItem(
        id: 1,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
    NotesItem(
        id: 2,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
    NotesItem(
        id: 3,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
    NotesItem(
        id: 4,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
    NotesItem(
        id: 5,
        title: 'Flutter is ',
        note:
            'Flutter is a mobile App SDK by Google which helps in creating modern mobile apps for iOS and Android using a single(almost) code base. It’s a new entrant in the cross platform mobile application development and unlike other frameworks like React Native, it doesn’t use JavaScript but DART as a Programming Language.'),
  ];
}
