import 'package:flutter/material.dart';

class NoteItem extends StatelessWidget {
  final String title;
  final String note;
  final Function onTap;
  const NoteItem({
    Key key, this.title, this.note, this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFFffb3ba),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: ListTile(
            onTap: ()=>onTap(),
            onLongPress: () {},
            title: Text(title,style: TextStyle(fontSize: 20,color: Colors.black45,fontWeight: FontWeight.w500),),
            subtitle: Text(
                note,style:TextStyle(color: Colors.black45)),
          ),
        ),
      ),
    );
  }
}
