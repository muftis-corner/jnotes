import 'package:flutter/material.dart';

class TodoDetail extends StatefulWidget {
  final String title;
  final int id;

  const TodoDetail({Key key, this.title, this.id}) : super(key: key);
  @override
  _TodoDetailState createState() => _TodoDetailState();
}

class _TodoDetailState extends State<TodoDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todo Details'),
      ),
    );
  }
}