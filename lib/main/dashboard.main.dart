import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/resource/color.dart';
import 'package:jnotes/resource/size.dart';
import 'components/header.components.dart';
import 'components/noteCard.components.dart';
import 'components/todoCard.components.dart';

class DashboardView extends StatefulWidget {
  final String username;

  const DashboardView({Key key, this.username}) : super(key: key);
  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView>
    with SingleTickerProviderStateMixin {
  TabController dashboardTabController;

  @override
  void initState() {
    super.initState();
    dashboardTabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      
     floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightBlue,
        onPressed: () {},
        child: Icon(CupertinoIcons.pen,color: Theme.of(context)
                                      .primaryColor,),
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: height * (8.1 / 10),
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                ),
                child:
                    TabBarView(controller: dashboardTabController, children: [
                  ListView(
                    children: <Widget>[
                      // Padding(
                      //   padding: EdgeInsets.only(
                      //       top: height * (0.6 / 10), right: 16, left: 16),
                      //   child: Container(
                      //     decoration: BoxDecoration(
                      //         color: Colors.red,
                      //         borderRadius: SideRadius.card,
                      //         gradient: BackgroundGradient.main),
                      //     height: 150,
                      //     width: double.infinity,
                      //   ),
                      // ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Latest Todo',
                              style: Theme.of(context).textTheme.title.apply(
                                  color: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.5)),
                            ),
                            Text(
                              '12/02',
                              style: Theme.of(context).textTheme.title.apply(
                                  color: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.5)),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 0, bottom: 16.0, left: 16, right: 16),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            TodoCard(
                              userID: 1,
                              id: 1,
                              isCompleted: false,
                              title: 'telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telottelolet om telot telolet om telot telolet om telot ',
                            ),
                            TodoCard(
                              userID: 1,
                              id: 2,
                              isCompleted: false,
                              title: 'Hello Hokya',
                            ),
                            TodoCard(
                              userID: 1,
                              id: 2,
                              isCompleted: true,
                              title: 'test',
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  ListView(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: height * (0.6 / 10), right: 16, left: 16),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: SideRadius.card,
                              gradient: BackgroundGradient.reverse),
                          height: 150,
                          width: double.infinity,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Icons.label_important,
                                    color: Theme.of(context)
                                        .primaryColorDark
                                        .withOpacity(0.5)),
                                Text(
                                  'Priority Note',
                                  style: Theme.of(context)
                                      .textTheme
                                      .title
                                      .apply(
                                          color: Theme.of(context)
                                              .primaryColorDark
                                              .withOpacity(0.5)),
                                ),
                              ],
                            ),
                            Text(
                              '12/02',
                              style: Theme.of(context).textTheme.title.apply(
                                  color: Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.5)),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 0, bottom: 16.0, left: 16, right: 16),
                        child: Column(
                          children: <Widget>[
                            NoteCard(
                              title: 'Judul',
                              note: 'telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telottelolet om telot telolet om telot telolet om telot ',
                            ),
                            NoteCard(
                              title: 'Judul',
                              note: 'telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telottelolet om telot telolet om telot telolet om telot ',
                            ),
                            NoteCard(
                              title: 'Judul',
                              note: 'telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telot telolet om telottelolet om telot telolet om telot telolet om telot ',
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ])),
          ),
          new DashboardHeader(
            height: height,
            width: width,
            widget: widget,
            dashboardTabController: dashboardTabController,
          )
        ],
      ),
    );
  }
}
