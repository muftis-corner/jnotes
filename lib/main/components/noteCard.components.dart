import 'package:flutter/material.dart';
import 'package:jnotes/modules/notes/page/notes.editor.dart';
import 'package:jnotes/resource/routes.dart';
import 'package:jnotes/resource/size.dart';
import 'package:jnotes/service/navigationServices.dart';

class NoteCard extends StatelessWidget {
  final String title;
  final String note;
  
  const NoteCard({
    Key key, this.title, this.note,
  }) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Paddings.bottom,
      child: GestureDetector(
        onTap: ()=>navigatorPush(context, NotesEditorView(title:title,note:note)),
              child: Container(
          decoration: BoxDecoration(
              borderRadius: SideRadius.card,
              color: Theme.of(context).primaryColor),
          height: 150,
          child: Column(
            mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  title,
                  style:
                      Theme.of(context).textTheme.title,
                ),
              ),
              Container(
                  alignment: Alignment.topLeft,
                  height: 69,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0),
                    child: Text(
                      note,
                      textAlign: TextAlign.justify,
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 12,
                ),
                child: Row(
                  mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('12/02'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
