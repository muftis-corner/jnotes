import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/resource/color.dart';
import 'package:jnotes/resource/routes.dart';
import 'package:jnotes/service/navigationServices.dart';

import '../dashboard.main.dart';

class DashboardHeader extends StatelessWidget {
  const DashboardHeader({
    Key key,
    @required this.height,
    @required this.width,
    @required this.widget,
    @required this.dashboardTabController,
  }) : super(key: key);

  final double height;
  final double width;
  final DashboardView widget;
  final TabController dashboardTabController;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height * (2.3 / 10),
      width: width,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        gradient: BackgroundGradient.reverse,
        boxShadow: [
          BoxShadow(color: BaseColor.shadow, spreadRadius: 3, blurRadius: 10)
        ],
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            child: SafeArea(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 16,
                    left: 16,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text(
                          'Hello,',
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.title.apply(color:Theme.of(context).primaryColor),
                        ),
                        Text(
                          widget.username,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 35, fontWeight: FontWeight.w600, color:Theme.of(context).primaryColor),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12, right: 24.0),
                      child: GestureDetector(
                        onTap: () =>
                            navigatorPushNamed(context, Routes.profile),
                        child: Container(
                          height: height * (0.8 / 10),
                          width: height * (0.8 / 10),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              alignment: Alignment.center,
                              image: NetworkImage(
                                'https://instagram.fplm4-1.fna.fbcdn.net/v/t51.2885-19/s150x150/14540465_1158763494209972_1300598908493234176_a.jpg?_nc_ht=instagram.fplm4-1.fna.fbcdn.net&_nc_cat=108&_nc_ohc=sdj1XksqS-4AX9d9kds&oh=906747b9a3c9c22c2d0432779be78cb5&oe=5E8CBD06',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 20,
            child: TabBar(
                indicatorWeight: 2,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 80),
                labelColor: Theme.of(context).accentColor,
                labelStyle: TextStyle(
                  fontSize: 15,
                ),
                unselectedLabelColor: Theme.of(context).primaryColor,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BubbleTabIndicator(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 4),
                    indicatorHeight: 20,
                    tabBarIndicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: Theme.of(context).primaryColor),
                controller: dashboardTabController,
                tabs: [
                  Text(
                    'Todo',
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    'Note',
                    style: TextStyle(fontSize: 18),
                  ),
                ]),
          )
        ],
      ),
    );
  }
}
