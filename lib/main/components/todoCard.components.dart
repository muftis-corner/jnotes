import 'package:flutter/material.dart';
import 'package:jnotes/modules/todo/page/todo.editor.dart';
import 'package:jnotes/resource/routes.dart';
import 'package:jnotes/resource/size.dart';
import 'package:jnotes/service/navigationServices.dart';

class TodoCard extends StatefulWidget {
  const TodoCard({
    Key key,
    this.userID,
    this.id,
    this.title,
    this.isCompleted,
  }) : super(key: key);

  final int userID;
  final int id;
  final String title;
  final bool isCompleted;

  @override
  _TodoCardState createState() => _TodoCardState();
}

class _TodoCardState extends State<TodoCard> {
  bool isCompleted;
  TextDecoration completed;
  String completeText;

  void checkCompleted() {
    isCompleted = widget.isCompleted;
    if (isCompleted) {
      setState(() {
        completeText = 'Done';
        completed = TextDecoration.lineThrough;
      });
    } else {
      setState(() {
        completeText = 'Doing';
        completed = TextDecoration.none;
      });
    }
  }

  void changeStatus() {
    setState(() {
      switch (isCompleted) {
        case true:
          setState(() {
            isCompleted = false;
            completeText = 'Doing';
            completed = TextDecoration.none;
          });
          break;
        case false:
          setState(() {
            isCompleted = true;
            completeText = 'Done';
            completed = TextDecoration.lineThrough;
          });
          break;
        default:
      }
    });
  }

  @override
  void initState() {
    super.initState();
    checkCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Paddings.bottom,
      child: GestureDetector(
        onTap: ()=>navigatorPush(context, TodoDetail(title: widget.title,id: widget.id,)),
        onDoubleTap: ()=>changeStatus(),
              child: Container(
          decoration: BoxDecoration(
              borderRadius: SideRadius.card,
              color: Theme.of(context).primaryColor),
          height: 140,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 16.0, left: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      completeText,
                      style: Theme.of(context).textTheme.title,
                    ),
                    Checkbox(value: isCompleted, onChanged: (value) => changeStatus())
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16, left: 16, right: 16),
                child: Container(
                    alignment: Alignment.topLeft,
                    height: 50,
                    child: Text(
                      'telolet om telot telolet om telot telolet om telot telolet om telot ulul ',
                      textAlign: TextAlign.justify,
                      style: Theme.of(context).textTheme.title.apply(
                          decoration: completed,
                          fontSizeDelta: -1,
                          fontWeightDelta: -3),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
