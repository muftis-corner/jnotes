import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainResource{
  static List<MainMenuItem> mainMenu = [
    MainMenuItem('Note', IconData(0xF476, fontFamily: CupertinoIcons.iconFont, fontPackage: CupertinoIcons.iconFontPackage), '/note'),
    MainMenuItem('Todo', Icons.playlist_add_check, '/todo'),
  ];
}

class MainMenuItem{
  final String title;
  final IconData icon;
  final String route;

  MainMenuItem(this.title, this.icon, this.route);
}