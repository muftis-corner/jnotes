import 'package:flutter/material.dart';
import 'package:jnotes/resource/theme.dart';
import 'init/profile/profile.dart';
import 'modules/notes/page/notes.editor.dart';
import 'resource/session.dart';

import 'init/login/login.dart';
import 'init/root/root.dart';

import 'main/dashboard.main.dart';

import 'modules/notes/notes.main.dart';
import 'modules/todo/todo.main.dart';
import 'resource/routes.dart';

class Router extends StatefulWidget {
  @override
  _RouterState createState() => _RouterState();
}

class _RouterState extends State<Router> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.root,
      theme: lightTheme,
      routes: {
        //--------- init ------------
        Routes.root : (context) => RootView(),
        Routes.login: (context) => LoginView(),
        Routes.profile: (context) => ProfileView(),
        Routes.main: (context) => DashboardView(
              username: '${session.userame}',
            ),
        //-------- modules ----------
        Routes.note: (context) => NotesView(),
        Routes.noteEditor: (context) => NotesEditorView(),
        Routes.todo: (context) => TodoView(),
      },
    );
  }
}



