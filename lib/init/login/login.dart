import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:jnotes/init/login/model/tabbar.model.dart';
import '../../resource/color.dart';

import 'components/login.widget.dart';
import 'components/register.widget.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with SingleTickerProviderStateMixin{
  
  String welcomeMessage = 'Welcome back';

  TabController _tabController;

  @override
  void initState() { 
    super.initState();
    _tabController = TabController(length: 2,vsync: this,initialIndex: 0);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    timeDilation = 3.0;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),
                  height: height * (2.5 / 10),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: BaseColor.darkBlue,
                      gradient: BackgroundGradient.reverse,
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(50))),
                  height: height * (2.5 / 10),
                ),
                Positioned(
                  bottom: height * (0.8 / 10),
                  left: width * (0.8 / 10),
                  child: Text(
                    welcomeMessage,
                    style: TextStyle(
                        fontSize: 30,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Positioned(
                  bottom: height * (0.1 / 10),
                  left: width * (0.8 / 10),
                  child: TabBar(
                    
                    onTap: (value){
                      switch (value) {
                        case 0:
                          setState(() {
                            welcomeMessage = 'Welcome back';
                          });
                          break;
                        case 1:
                          setState(() {
                            welcomeMessage = 'Welcome';
                          });
                          break;
                        default:
                      }
                    },
                    isScrollable: true,
                    labelColor: Theme.of(context).accentColor,
                    unselectedLabelColor: Theme.of(context).primaryColor,
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicator: BubbleTabIndicator(
                      indicatorHeight: 25,
                      tabBarIndicatorSize: TabBarIndicatorSize.tab,
                      indicatorColor: Theme.of(context).primaryColor
                    ),
                    tabs: [
                      Tab(text: 'Sign in',),
                      Tab(text: 'Sign up',),
                      ],
                    controller: _tabController,
                  ),
                )
              ],
            ),
            Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: BaseColor.darkBlue,
                  ),
                  height: height * (6 / 10),
                ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50))),
                  height: height * (6 / 10),
                  child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: <Widget>[
                      LoginWidget(),
                      SignUpWidget(),
                    ],
                  ),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),
                  height: height * (1.5 / 10),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: BaseColor.darkBlue,
                      gradient: BackgroundGradient.main,
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(50))),
                  height: height * (1.5 / 10),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
