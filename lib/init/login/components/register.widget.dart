import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../resource/color.dart';
import '../../../resource/components/common.components.dart';

class SignUpWidget extends StatefulWidget {
  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpWidget> {
  
  TextEditingController usernameSignUpController;
  
  TextEditingController firstNameSignUpController;
  TextEditingController lastNameSignUpController;
  TextEditingController emailSignUpController;
  TextEditingController passwordSignUpController;
  TextEditingController retypePasswordSignUpController;
  

  FocusNode usernameSignUpFocus;
  FocusNode firstNameSignUpFocus;
  FocusNode lastNameSignUpFocus;
  FocusNode passwordSignUpFocus;
  FocusNode retypePasswordSignUpFocus;
  FocusNode emailSignUpFocus;

  @override
  void initState() {
    super.initState();

    usernameSignUpController = TextEditingController();
    firstNameSignUpController = TextEditingController();
    lastNameSignUpController = TextEditingController();
    emailSignUpController = TextEditingController();
    passwordSignUpController = TextEditingController();
    retypePasswordSignUpController = TextEditingController();
    
    usernameSignUpFocus = FocusNode();
    firstNameSignUpFocus = FocusNode();
    lastNameSignUpFocus = FocusNode();
    emailSignUpFocus = FocusNode();
    passwordSignUpFocus = FocusNode();
    retypePasswordSignUpFocus = FocusNode();
  }

  @override
  void dispose() { 
    usernameSignUpController.dispose();
    passwordSignUpController.dispose();
    emailSignUpController.dispose();
    firstNameSignUpController.dispose();
    lastNameSignUpController.dispose();
    retypePasswordSignUpController.dispose();

    usernameSignUpFocus.dispose();
    firstNameSignUpFocus.dispose();
    lastNameSignUpFocus.dispose();
    emailSignUpFocus.dispose();
    passwordSignUpFocus.dispose();
    retypePasswordSignUpFocus.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(36.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: firstNameSignUpController,
            focusNode: firstNameSignUpFocus,
            autofocus: true,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(lastNameSignUpFocus),
            decoration: InputDecoration(
              labelText: 'First Name',
            ),
          ),
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: lastNameSignUpController,
            focusNode: lastNameSignUpFocus,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(usernameSignUpFocus),
            decoration: InputDecoration(
              labelText: 'lastName',
            ),
          ),
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: usernameSignUpController,
            focusNode: usernameSignUpFocus,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(emailSignUpFocus),
            decoration: InputDecoration(
              labelText: 'Username',
            ),
          ),
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: emailSignUpController,
            focusNode: emailSignUpFocus,
            keyboardType: TextInputType.emailAddress,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(passwordSignUpFocus),
            decoration: InputDecoration(
              labelText: 'E-mail',
            ),
          ),
          TextFormField(
            style: TextStyle(color: Theme.of(context).primaryColor),
            controller: passwordSignUpController,
            focusNode: passwordSignUpFocus,
            obscureText: true,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(retypePasswordSignUpFocus),
            decoration: InputDecoration(
              labelText: 'Password',
            ),
          ),
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: retypePasswordSignUpController,
            focusNode: retypePasswordSignUpFocus,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Re-type Password',
            ),
          ),          
          Spacing(vertical: height*(0.3/10)),
          Button(
            color: Theme.of(context).accentColor,
             onPressed: (){},
             text: 'Sign Up',
             textColor: Theme.of(context).primaryColor,

          )
        ],
      ),
    );
  }
}
