import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/init/login/controller/login.controller.dart';
import '../../../resource/color.dart';
import '../../../resource/components/common.components.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  TextEditingController usernameLoginController;
  TextEditingController passwordLoginController;

  FocusNode usernameLoginFocus;
  FocusNode passwordLoginFocus;

  @override
  void initState() {
    super.initState();
    usernameLoginController = TextEditingController();
    passwordLoginController = TextEditingController();
    usernameLoginFocus = FocusNode();
    passwordLoginFocus = FocusNode();
  }

  @override
  void dispose() {
    usernameLoginController.dispose();
    passwordLoginController.dispose();
    usernameLoginFocus.dispose();
    passwordLoginFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(36.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: usernameLoginController,
            focusNode: usernameLoginFocus,
            autofocus: true,
            onEditingComplete: () =>
                FocusScope.of(context).requestFocus(passwordLoginFocus),
            decoration: InputDecoration(
              labelText: 'Username',
            ),
          ),
          TextFormField(
            style: TextStyle(color:Theme.of(context).primaryColorDark),
            controller: passwordLoginController,
            focusNode: passwordLoginFocus,
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Password',
            ),
          ),
          Spacing(vertical: 16),
          Align(
              alignment: Alignment.centerRight,
              child: LinkButton(onTap: (){}, label: 'Forgotten Password ?')
              ),
          
          Spacing(vertical: height*(0.3/10)),
          Button(
            color: Theme.of(context).accentColor,
             onPressed: (){
               LoginController.goToDashboard(context);
             },
             text: 'Sign In',
             textColor: Theme.of(context).primaryColor,
          ),
          Spacing(vertical: 16),
          Align(
              alignment: Alignment.center,
              child: LinkButton(onTap: (){
                LoginController.goToDashboard(context);
              }, label: 'Skip For Now')
              ),
          
        ],
      ),
    );
  }
}
