import 'package:flutter/material.dart';
import 'package:jnotes/resource/routes.dart';
import 'package:jnotes/service/navigationServices.dart';

class LoginController{
  static void goToDashboard(BuildContext context){
    navigatorPushRoot(context, Routes.main);
  }
}