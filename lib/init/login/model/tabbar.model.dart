
import 'package:flutter/material.dart';
import 'package:jnotes/init/login/components/login.widget.dart';

class SignInItem {
  final String title;
  final Widget child;
  
  SignInItem(this.title, this.child);

  static List<SignInItem> signInMenu= <SignInItem>[
   SignInItem(
     'Sign in',
     LoginWidget(),
   ),
   SignInItem(
     'Sign up',
     LoginWidget(),
   ),
  ];
}

