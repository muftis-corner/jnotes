import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jnotes/resource/color.dart';
import 'package:jnotes/resource/components/common.components.dart';
import 'package:jnotes/resource/image.dart';
import 'package:jnotes/service/navigationServices.dart';
import 'package:flutter/scheduler.dart';

class RootView extends StatefulWidget {
  @override
  _RootViewState createState() => _RootViewState();
}

class _RootViewState extends State<RootView> {
  void init() {
    timeDilation = 1.0;
    Future.delayed(
        Duration(seconds: 3),
        () => navigatorPushRoot(context, '/login')
            );
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double size = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Hero(
              child: SizedBox(
                height: size / 5,
                width: size / 5,
                child: Images.logo,
              ),
              tag: 'logo',
            ),
            Spacing(
              vertical: size / 5,
            ),
            CupertinoActivityIndicator(
              radius: 10,
            )
          ],
        ),
      ),
    ));
  }
}
